create table users
(
    user_id            bigint auto_increment
        primary key,
    created_by         varchar(255) null,
    created_date       datetime(6)  null,
    last_modified_by   varchar(255) null,
    last_modified_date datetime(6)  null,
    active             bit          null,
    alamat             varchar(255) null,
    email              varchar(255) null,
    nama               varchar(10)  null,
    nim                varchar(10)  null,
    password           varchar(255) null,
    phone              varchar(12)  null,
    constraint UK_6dotkott2kjsp8vw4d0m25fb7
        unique (email),
    constraint UK_du5v5sr43g5bfnji4vb8hg5s3
        unique (phone),
    constraint UK_h5k5eqgbwmq6sh7k3nqfr1439
        unique (nim)
);


-- password = 1234
INSERT INTO `spring-boot-perpustakaan`.users (user_id, created_by, created_date, last_modified_by, last_modified_date, active, alamat, email, nama, nim, password, phone) VALUES (1, null, null, null, null, true, 'dago', 'admin@mail.com', 'admin', '-', '$2a$10$3nVQ6VBQOHzFtBA6ZS9KkuRH3g20qdLDDGk/2wQuWx701nDakknIe', '081234560526');

create table roles
(
    role_id bigint auto_increment
        primary key,
    role    varchar(255) null
);

INSERT INTO `spring-boot-perpustakaan`.roles (role_id, role) VALUES (1, 'ADMIN');

create table user_role
(
    user_id bigint not null,
    role_id bigint not null,
    primary key (user_id, role_id),
    constraint FKj345gk1bovqvfame88rcx7yyx
        foreign key (user_id) references users (user_id),
    constraint FKt7e7djp752sqn6w22i6ocqy6q
        foreign key (role_id) references roles (role_id)
);

INSERT INTO `spring-boot-perpustakaan`.user_role (user_id, role_id) VALUES (1, 1);