package id.build.sistemperpustakaan.model;

import com.sun.istack.Nullable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;

/**
 * Project Name     : sistem-perpustakaan
 * Date Time        : 4/30/2020
 *
 * @author Teten Nugraha
 */

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Entity
@Table(name = "users")
public class User extends Auditable<String> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", unique = true)
    private Long id;

    @Length(min = 5, max = 10, message = "*Minimal 5 karakter dan Maksimal 10 karakter")
    @NotEmpty(message = "*Please provide a NIM")
    @Column(name = "nim", unique = true)
    private String nim;

    @Length(min = 3, max = 10, message = "*Minimal 3 karakter dan Maksimal 30 karakter")
    @NotEmpty(message = "*Please provide a Nama")
    private String nama;

    @Column(name = "password")
    @Length(min = 5, message = "*Your password must have at least 5 characters")
    @NotEmpty(message = "*Please provide your password")
    private String password;

    @Column(name = "email", unique = true)
    @Email(message = "*Please provide a valid Email")
    @NotEmpty(message = "*Please provide an email")
    private String email;

    @Column(name = "phone", unique = true)
    @NotEmpty(message = "*Please provide an phone")
    @Length(min = 11, max = 12, message = "*Minimal 11 karakter dan Maksimal 12 karakter")
    private String phone;

    @Column(name = "alamat")
    @Nullable
    private String alamat;

    @Column(name = "active")
    private boolean active;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(name = "user_role",
            joinColumns = {
                    @JoinColumn(name = "user_id", referencedColumnName = "user_id",
                            nullable = false, updatable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "role_id", referencedColumnName = "role_id",
                            nullable = false, updatable = false)})
    private Set<Role> roles = new HashSet<>();



}
