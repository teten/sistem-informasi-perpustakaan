package id.build.sistemperpustakaan.web;

import id.build.sistemperpustakaan.model.User;
import id.build.sistemperpustakaan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

/**
 * Project Name     : sistem-informasi-perpustakaan
 * Date Time        : 5/1/2020
 *
 * @author Teten Nugraha
 */

@Controller
public class AuthController {

    @Autowired
    private UserService userService;


    @GetMapping("/registration")
    public ModelAndView registration() {
        ModelAndView modelAndView = new ModelAndView();
        User user = new User();
        modelAndView.addObject("user", user);
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    @PostMapping("/registration")
    public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        User userExists = userService.findByNama(user.getNama());
        if(userExists != null) {
            bindingResult
                    .rejectValue("userName","error.user", "There is already a user registered with the user name provided");
        }

        if(bindingResult.hasErrors()) {
            modelAndView.setViewName("registration");
        }else{
            userService.saveUser(user);
            modelAndView.addObject("successMessage","user has been registered successfully");
            modelAndView.addObject("user",user);
            modelAndView.setViewName("registration");

        }
        return modelAndView;
    }
}
