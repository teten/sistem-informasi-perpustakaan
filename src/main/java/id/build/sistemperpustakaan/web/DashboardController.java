package id.build.sistemperpustakaan.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Project Name     : sistem-informasi-perpustakaan
 * Date Time        : 5/5/2020
 *
 * @author Teten Nugraha
 */

@Controller
public class DashboardController {

    @GetMapping("/")
    public String index() {
        return "dashboard/index";
    }

}