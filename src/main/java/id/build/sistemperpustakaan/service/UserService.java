package id.build.sistemperpustakaan.service;

import id.build.sistemperpustakaan.model.User;

/**
 * Project Name     : sistem-informasi-perpustakaan
 * Date Time        : 5/1/2020
 *
 * @author Teten Nugraha
 */

public interface UserService {
    User findUserByEmail(String email);
    User findByNama(String nama);
    User saveUser(User user);
}
