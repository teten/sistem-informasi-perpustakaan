package id.build.sistemperpustakaan.service.impl;

import id.build.sistemperpustakaan.model.Role;
import id.build.sistemperpustakaan.model.User;
import id.build.sistemperpustakaan.repository.RoleRepository;
import id.build.sistemperpustakaan.repository.UserRepository;
import id.build.sistemperpustakaan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;

import static id.build.sistemperpustakaan.utils.Constants.Role.USER_ROLE;

/**
 * Project Name     : sistem-informasi-perpustakaan
 * Date Time        : 5/1/2020
 *
 * @author Teten Nugraha
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    @Override
    public User findByNama(String nama) {
        return userRepository.findUserByNama(nama);
    }

    @Override
    public User saveUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setActive(true);
        Role role = roleRepository.findByRole(USER_ROLE);
        user.setRoles(new HashSet<>(Arrays.asList(role)));
        return userRepository.save(user);
    }
}
