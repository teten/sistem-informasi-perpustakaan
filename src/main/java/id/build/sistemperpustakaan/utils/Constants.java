package id.build.sistemperpustakaan.utils;

/**
 * Project Name     : sistem-informasi-perpustakaan
 * Date Time        : 5/1/2020
 *
 * @author Teten Nugraha
 */

public class Constants {

    public static class Role {
        public final static String USER_ROLE = "USER";
        public final static String ADMIN_ROLE = "ADMIN";
    }

}
