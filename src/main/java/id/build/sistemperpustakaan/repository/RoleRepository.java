package id.build.sistemperpustakaan.repository;

import id.build.sistemperpustakaan.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Project Name     : sistem-informasi-perpustakaan
 * Date Time        : 5/1/2020
 *
 * @author Teten Nugraha
 */

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByRole(String role);
}
