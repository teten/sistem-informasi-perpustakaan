package id.build.sistemperpustakaan.repository;

import id.build.sistemperpustakaan.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Project Name     : sistem-informasi-perpustakaan
 * Date Time        : 5/1/2020
 *
 * @author Teten Nugraha
 */

public interface UserRepository extends JpaRepository<User, Long> {

    User findUserByEmail(String email);
    User findUserByNama(String nama);

}
