package id.build.sistemperpustakaan.config;

import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Project Name     : sistem-informasi-perpustakaan
 * Date Time        : 5/5/2020
 *
 * @author Teten Nugraha
 */

@Configuration
public class ThymeleafConfig {

    @Bean
    public LayoutDialect layoutDialect() {
        return new LayoutDialect();
    }
}
